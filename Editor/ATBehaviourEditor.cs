﻿using UdonSharpEditor;
using UnityEditor;

#if VUDON_LOGGER
using Varneon.VUdon.Logger.Abstract;
#endif

#pragma warning disable CS0612

namespace ArchiTech.SDK.Editor
{
    /// <summary>
    /// Base class for custom editors used across the ArchiTech namespace.
    /// Simplifies the setup and boilerplate for more complex editors.
    /// </summary>
    public abstract class ATBehaviourEditor : ATBaseEditor
    {
        private ATBehaviour _atScript;
#if VUDON_LOGGER
        private UdonLogger[] _udonLoggers;
        private string[] _udonLoggerNames;
#endif


        protected override void DrawInspector()
        {
            _atScript = (ATBehaviour)target;
            serializedObject.Update();
            LoadData();
            if (!init)
            {
                init = true;
                InitData();
                HandleSave();
            }

            Header();
            if (autoRenderHeader && UdonSharpGUI.DrawDefaultUdonSharpBehaviourHeader(_atScript)) return;
            showHints = EditorGUILayout.Toggle(GetPropertyLabel(this, nameof(showHints)), showHints);
            using (ChangeCheckScope)
            {
#if VUDON_LOGGER
                // DrawVariablesByNameAsType(typeof(UdonLogger), nameof(_atScript._logger));
                if (_udonLoggers == null) _udonLoggers = ATEditorUtility.GetComponentsInSceneWithDistinctNames<UdonLogger>(out _udonLoggerNames);
                DrawVariableWithDropdown(nameof(_atScript._logger));
#endif
                VariablesDrawn(nameof(_atScript._logger));
                DrawVariablesByName(nameof(_atScript._maxLogLevel));
                DrawLine();
                RenderChangeCheck();
                if (autoRenderVariables) DrawVariables();
                HandleSave();
            }

            Footer();
        }

        protected static void DisplayTemplateError() => EditorGUILayout.HelpBox(I18n.Tr("Template components MUST be a descendant of the containing template object."), MessageType.Error);
    }
}